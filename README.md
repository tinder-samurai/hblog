# **HBlog** [![codebeat badge](https://codebeat.co/badges/2bdd3b2a-0568-4514-a3ce-f875232768f9)](https://codebeat.co/projects/github-com-henryco-hblog-master)
### Simple blog engine based on Spring (MVC + JPA + SECURITY) 


![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/1.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/2.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/3.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/4.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/5.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/6.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/9.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/10.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/12.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/13.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/14.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/15.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/16.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/17.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/7.png)

![promo](https://bitbucket.org/tinder-samurai/hblog/raw/93ad676d5558895c6904c119b439883a838232ba/promo/8.png)

